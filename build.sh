#!/bin/bash

set -e

export SCRIPT_DIR="$(readlink -f "$(dirname "$0")")"

# default values for flags
export APPLY_NAMING_SCHEME=0
export RUN_TESTS=0
export CLEANUP_IMAGES=0
export PUBLISH_IMAGES=0
export DOCKER_IP="127.0.0.1"
export TEST_TIMEOUT=30
export DEB_URL="repository"
# unset just in case
unset JRIVER_CONFIG_DIR
unset CONTAINER_MAC_ADDRESS

usage() {
    if [ "$*" ]; then
        echo "$*"
        echo
    fi

    echo "usage: $( basename $0 ) [OPTIONS]... DOCKER_TAG

Arguments:
  DOCKER_TAG          Defines the container flavor to build.  Valid values are:
$(find "$SCRIPT_DIR"/versions -type f -printf '                        %f\n' | sort)
Options:
  -h, --help          Display this help.
  -n, --name          Applies naming scheme to tag 26.0.15-latest-4. Starts container to determine version.
  -t, --test          Runs tests (requires python3 to be installed).
  --test-timeout      Set common timeout in tests (CI runners might be real slow at times)
  -c, --cleanup       Deletes all images that match the repository name before and after building.
  -p, --publish       Pushes images to repository.
  --docker-ip <ip>    Specify a docker ip (defaults to 127.0.0.1). For tests etc. to work when using remote docker engine.
  --deb-url <url>     Supply .deb file via url to use instead of repository.
  --config-dir        Supply config directory to use for temporary containers created in tests and to determine jriver version.
  --mac-address       Supply custom mac address to use for temporary containers created in tests and to determine jriver version.
"
}

# Parse arguments.
while [[ $# > 0 ]]
do
    key="$1"

    case $key in
        -h|--help)
            usage
            exit 0
            ;;
        -n|--name)
            export APPLY_NAMING_SCHEME=1
            ;;
        -t|--test)
            export RUN_TESTS=1
            ;;
        -c|--cleanup)
            export CLEANUP_IMAGES=1
            ;;
        -p|--publish)
            export PUBLISH_IMAGES=1
            ;;
        --docker-ip)
            shift
            export DOCKER_IP="$1"
            ;;
        --test-timeout)
            shift
            export TEST_TIMEOUT=$1
            ;;
        --deb-url)
            shift
            export DEB_URL="$1"
            ;;
        --config-dir)
            shift
            export JRIVER_CONFIG_DIR="$1"
            ;;
        --mac-address)
            shift
            export CONTAINER_MAC_ADDRESS="$1"
            ;;
        -*)
            usage "ERROR: Unknown argument: $key"
            exit 1
            ;;
        *)
            break
            ;;
    esac
    shift
done

DOCKER_TAG="$1"

[ -z "$DOCKER_TAG" ] && usage "ERROR: Docker tag not supplied." && exit 1
if [ ! -f "$SCRIPT_DIR"/versions/"$DOCKER_TAG" ]; then
    usage "ERROR: Invalid docker tag: $DOCKER_TAG."
    exit 1
fi

# Export build variables.
if [ -z "$CI_REGISTRY_IMAGE_NAME" ]
then
    export DOCKER_REPO=shiomax/jrivermc28-testing
else
    export DOCKER_REPO="$CI_REGISTRY_IMAGE_NAME"
fi
export IMAGE_VERSION=$(cat rootfs/VERSION)
export DOCKER_TAG=$DOCKER_TAG

[ "$CLEANUP_IMAGES" == "1" ] && scripts/cleanup.sh
scripts/build_image.sh
[ "$CLEANUP_IMAGES" == "1" ] && scripts/cleanup.sh

# Unset environment variables
unset DOCKER_REPO
unset IMAGE_VERSION
unset DOCKER_TAG
unset APPLY_NAMING_SCHEME
unset RUN_TESTS
unset CLEANUP_IMAGES
unset PUBLISH_IMAGES
unset CUSTOM_DEB_FILE
unset SCRIPT_DIR