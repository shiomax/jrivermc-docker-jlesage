#!/bin/sh
export HOME=/config

echo '--------------------------------------------------'
echo 'Unofficial JRiver MC Image.'
echo "Image Version: $(cat /VERSION)"
echo ''
echo 'Source: https://gitlab.com/shiomax/jrivermc-docker'
echo 'Dockerhub: https://hub.docker.com/u/shiomax'
echo ''
echo '--------------------------------------------------'

lockFile="/tmp/BLOCK_MC_STARTUP"
while test -f "$lockFile"; do
    echo "NOTE: MC startup is blocked. Remove file '$lockFile' to unblock."
    sleep 5s
done;

mediacenter%%MC_VERSION%% /mediaserver
