#!/usr/bin/with-contenv sh
while read line; do
    hex=$(echo $line | cut -c41-)
    if [ "$hex" = "0x0" ]; then
        
        winlist=$(wmctrl -l)
        while [ -z "$winlist" ]; do
            sleep 1s 
            winlist=$(wmctrl -l)
        done;

        jriver_main_window=$(echo $winlist | head -n 1 | awk '{print $1}')
        window_state=$(xprop -id $jriver_main_window | grep "_NET_WM_STATE(ATOM)" | cut -c23-)
        if [ $(echo "$window_state" | grep -c "_NET_WM_STATE_HIDDEN") != 0 ]; then
            wmctrl -i -a $jriver_main_window
        fi
    fi
done