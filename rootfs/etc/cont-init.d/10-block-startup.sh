#!/usr/bin/with-contenv sh

set -e # Exit immediately if a command exits with a non-zero status.
set -u # Treat unset variables as an error.

log() {
    RED='\033[0;31m'
    NC='\033[0m' # No Color
    printf "[cont-init.d] $(basename $0): ${RED}$*${NC}\n"
}

if [ "${BLOCK_MC_STARTUP:-0}" -eq 1 ]; then
    log "JRiver startup will be blocked. Unset BLOCK_MC_STARTUP to undo."
    touch /tmp/BLOCK_MC_STARTUP
fi