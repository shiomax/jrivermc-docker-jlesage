# Contributing

Any contributions are welcome. Wether it´s code related, or errors/improvements in the documentation.

It´s recommended that you do your changes in the ```dev``` branch as what´s in the ```master``` branch will be used to continiously rebuild the docker image.
Changes to the ```master``` branch may get rejected for that reason.

1. Fork repository on ```gitlab.com``` into your own user account ([official gitlab.com guide](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork))
2. Clone project ```git clone <forked_repo_url>```
3. Checkout dev ```git checkout dev```
4. Make your changes
5. Push to your repository ```git add -A && git commit -m "Descriptive Commit Message" && git push -u origin dev```
6. Make a merge request ([official gitlab.com guide](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html))

# Building the image

**Build Requirements**
- A Linux environment is mostly mandatory (macos might work but I did not try it). If you are running Windows you can install a distro of your choice in WSL. Such as Debian or Ubuntu.
- Tests require ```python3``` to be installed, otherwise it´s not required
- Docker needs to be installed, if you are running WSL1 it cannot run the docker deamon, but it can connect to either the Windows docker deamon, or to a remote Docker engine running on a Linux box somewhere. You will still have to install docker on it to get the CLI. And the build script also has an option where you can specify the ip of the docker box when using a remote one. This is mostly only used for tests.

You can build the image locally using the ```build.sh``` script.
Run ```./build.sh --help``` to get a breakdown of all the currently available options.

Simple build example: ```./build.sh 26-latest-debian-10-amd64```