ARG baseimage

FROM ${baseimage}

# JRiver Version tag (latest, stable or beta)
ARG jriver_tag

# JRiver Release Version (25, 26 etc.)
ARG jriver_release

# Image Version of the build
ARG image_version

# .deb download URL, if set to "repository" the JRiver repository will be used
ARG deb_url

RUN add-pkg gnupg2 libxss1 wmctrl xdotool ca-certificates inotify-tools libgbm1 ffmpeg

# Install JRiver
RUN \
    add-pkg --virtual build-dependencies wget && \
    # Install from Repository
    if [ "${deb_url}" = "repository" ]; then \
        echo "Installing JRiver from repository ${jriver_release}:${jriver_tag}" && \
        wget -q "http://dist.jriver.com/mediacenter@jriver.com.gpg.key" -O- | apt-key add - && \
        wget http://dist.jriver.com/${jriver_tag}/mediacenter/mediacenter${jriver_release}.list -O /etc/apt/sources.list.d/mediacenter${jriver_release}.list && \
        apt-get update && \
        add-pkg mediacenter${jriver_release}; \
    # Install from .deb URL
    else \
        echo "Installing JRiver from URL: ${deb_url}" && \
        wget -q -O "jrivermc.deb" ${deb_url} && \
        add-pkg "./jrivermc.deb"; \
    fi && \
    # Cleanup
    del-pkg build-dependencies && \
    rm -rf /tmp/* /tmp/.[!.]*

# Add rootfs
COPY rootfs/ /

VOLUME ["/config"]

# Application Icon
RUN \
    APP_ICON_URL=https://gitlab.com/shiomax/jrivermc-docker-jlesage/raw/master/assets/Application.png && \
    install_app_icon.sh "$APP_ICON_URL"

# Various configuration vars
ENV KEEP_APP_RUNNING=1 \
    DISPLAY_WIDTH=1280 \
    DISPLAY_HEIGHT=768 \
    APP_NAME="JRiver MediaCenter ${jriver_release}" \
    MAXIMIZE_POPUPS=0 \
    S6_KILL_GRACETIME=8000

# Replace stuff in various files
RUN \
    # Modify startapp.sh and rc.xml with JRiver version
    sed-patch s/%%MC_VERSION%%/${jriver_release}/g \
        /startapp.sh && \
    sed-patch s/%%MC_VERSION%%/${jriver_release}/g \
        /etc/xdg/openbox/rc.xml && \
    # Remove line from statoverride (fix apt error 'unknown system group 'messagebus' in statoverride file')
    sed-patch '/messagebus 4754/d' /var/lib/dpkg/statoverride

EXPOSE 5800 5900 52100 52101 52199 1900/udp

# Metadata.
LABEL \
      org.label-schema.name="jrivermc${jriver_release}" \
      org.label-schema.description="Docker image for JRiver Media Center ${jriver_release}." \
      org.label-schema.version="${image_version}" \
      org.label-schema.vcs-url="https://gitlab.com/shiomax/jrivermc-docker" \
      org.label-schema.schema-version="1.0"
