#!/bin/bash

usage() {
    if [ "$*" ]; then
        echo "$*"
        echo
    fi

    echo "usage: $( basename $0 ) [OPTIONS]

Options:
  -h, --help          Display this help.
  -c, --channel       Channel part of the url (default: v26/latest)
  -v, --version       JRiver version (default: 26)
  --arch              Architecture (default: amd64)
  --min               Min version for iterator (refers to last number of jriver version 26.0.0) (default: 0).
  --max               Max version for iterator (refers to last number of jriver version 26.0.200) (default: 200).
"
}

channel="v26/latest"
version=26
arch="amd64"
min_version=19
max_version=100

# Parse arguments.
while [[ $# > 0 ]]
do
    key="$1"

    case $key in
        -h|--help)
            usage
            exit 0
            ;;
        -c|--channel)
            shift
            channel="$1"
            ;;
        -v|--version)
            shift
            version="$1"
            ;;
        --min)
            shift
            min_version="$1"
            ;;
        --max)
            shift
            max_version="$1"
            ;;
        --arch)
            shift
            arch="$1"
            ;;
        -*)
            usage "ERROR: Unknown argument: $key"
            exit 1
            ;;
        *)
            break
            ;;
    esac
    shift
done

for v in $(seq $min_version $max_version); do
    if [[ $channel == *"/beta" ]]; then
        # URL for beta versions is a bit different
        url="https://files.jriver.com/mediacenter/test/MediaCenter-$version.0.$v-$arch.deb"
    else
        url="https://files.jriver.com/mediacenter/channels/$channel/MediaCenter-$version.0.$v-$arch.deb"
    fi;
    status=$(curl --silent --head "$url" | head -n 1)

    if [ $(echo $status | grep --count "200") == 1 ] || false; then
        echo "$version $version.0.$v $url"
    fi;
done