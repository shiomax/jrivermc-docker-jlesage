#!/bin/bash

# Note started from project root

CI_BUILD_EXTRA_OPS=""

# Config Dir Option
if [ -n "$CI_JRIVER_CONFIG_DIRECTORY" ]; then
    CI_BUILD_EXTRA_OPS+="--config-dir $CI_JRIVER_CONFIG_DIRECTORY"
fi

# Mac Address Option
if [ -n "$CI_CONTAINER_MAC_ADDR" ]; then
    if [[ ! -z $CI_BUILD_EXTRA_OPS ]]; then
        CI_BUILD_EXTRA_OPS+=" "
    fi;
    CI_BUILD_EXTRA_OPS+="--mac-address $CI_CONTAINER_MAC_ADDR"
fi

# Deb Url option
if [ -n "$CI_DEB_URL" ]; then
    if [[ ! -z $CI_BUILD_EXTRA_OPS ]]; then
        CI_BUILD_EXTRA_OPS+=" "
    fi;
    CI_BUILD_EXTRA_OPS+="--deb-url $CI_DEB_URL"
fi

./build.sh --name --cleanup --publish --test --test-timeout 120 $CI_BUILD_EXTRA_OPS "$CI_BUILD_VERSION"