#!/bin/bash

# load variable from config file
source versions/"$DOCKER_TAG"

TEMP_CONTAINER_NAME="temporary_container_jriver_docker"

function build_container() {
    echo "--------------------------------"
    echo "Starting build"
    echo "--------------------------------"
    echo "REPOSITORY:     $DOCKER_REPO"
    echo "DOCKER_TAG:     $DOCKER_TAG"
    echo "JRIVER_TAG:     $JRIVER_TAG"
    echo "JRIVER_RELEASE: $JRIVER_RELEASE"
    echo "BASEIMAGE:      $BASEIMAGE"
    echo "NAME_IMAGES:    $APPLY_NAMING_SCHEME"
    echo "PUBLISH_IMAGES: $PUBLISH_IMAGES"
    echo "DOCKER_IP:      $DOCKER_IP"
    echo "DEB_URL:        $DEB_URL"
    echo "ARCH:           $ARCH"
    echo "Config_Dir:     $JRIVER_CONFIG_DIR"
    echo "Mac Addr:       $CONTAINER_MAC_ADDRESS"
    echo "--------------------------------"

    docker build \
        --no-cache --pull \
        --build-arg jriver_tag="$JRIVER_TAG" \
        --build-arg jriver_release="$JRIVER_RELEASE" \
        --build-arg baseimage="$BASEIMAGE" \
        --build-arg image_version="$IMAGE_VERSION" \
        --build-arg deb_url="$DEB_URL" \
        -t "$DOCKER_REPO:$DOCKER_TAG" .
}

function stop_container() {
    # Remove container if it exists
    docker rm -f $TEMP_CONTAINER_NAME > /dev/null 2>&1
}

function start_container() {
    # Stop container if started already
    stop_container

    echo "Starting temporary container..."

    DOCKER_EXTRA_OPTS=""
    if [ -n "$JRIVER_CONFIG_DIR" ]; then
        echo "Config directory was defined as $JRIVER_CONFIG_DIR"
        # Skipping directory exists check if using a remote docker host as the directory won't be expected to exist locally
        if [ -d "$JRIVER_CONFIG_DIR" ] || [ "$DOCKER_IP" != "127.0.0.1" ]; then
            DOCKER_EXTRA_OPTS+="-v $JRIVER_CONFIG_DIR:/config:rw"
        else
            "Directory $JRIVER_CONFIG_DIR was not found. Skipping volume mount configuration..."
        fi;
    fi;
    if [ -n "$CONTAINER_MAC_ADDRESS" ]; then
        echo "Mac address was defined as $CONTAINER_MAC_ADDRESS"
        if [[ ! -z $DOCKER_EXTRA_OPTS ]]; then
            DOCKER_EXTRA_OPTS+=" "
        fi;
        DOCKER_EXTRA_OPTS+="--mac-address=$CONTAINER_MAC_ADDRESS"
    fi;

    # Start container
    docker run -d \
        --name=$TEMP_CONTAINER_NAME \
        --net=bridge \
        -p 52199:52199 \
        $DOCKER_EXTRA_OPTS \
        "$DOCKER_REPO:$DOCKER_TAG"
    
    echo "Waiting 30 seconds..."
    # Giving the container some time to start
    sleep 30
}

function docker_tag_exists() {
    curl --silent -f -lSL https://hub.docker.com/v2/repositories/$DOCKER_REPO/tags/$1 &> /dev/null
}

function determine_jriver_version() {

    start_container

    # Getting JRiver version from MCWS
    JRIVER_VERSION=$(curl -Ls "$DOCKER_IP:52199/MCWS/v1/Alive" | grep 'ProgramVersion' | grep -o '[0-9]\+.[0-9]\.[0-9]\+')

    if [ "$JRIVER_VERSION" == "" ]; then
        echo "JRiver version could not be determined. Exiting"
        stop_container
        exit 1
    fi;

    # Release tag in form of 25.0.50-stable-1
    RELEASE_TAG="$JRIVER_VERSION-$JRIVER_TAG-$IMAGE_VERSION"
    echo "RELEASE TAG: $RELEASE_TAG"

    # Check if tag really is what's expected
    if ! [[ $RELEASE_TAG =~ ^${JRIVER_RELEASE}.[0-9]+.[0-9]+-(latest|stable|beta)-[0-9]+$ ]]; then
        echo "RELEASE TAG is invalid. Exiting."
        stop_container
        exit 1
    fi;

    stop_container
}

function publish_rolling() {
    echo "Publishing rolling container"
    docker tag "$DOCKER_REPO:$DOCKER_TAG" "$DOCKER_REPO:$JRIVER_TAG"
    docker push "$DOCKER_REPO:$JRIVER_TAG"
}

function publish_versioned() {
    if docker_tag_exists $RELEASE_TAG; then
        echo "Image exists already. Skipping push."
    else
        echo "Image does not exist. Pushing versioned container."
        docker tag "$DOCKER_REPO:$DOCKER_TAG" "$DOCKER_REPO:$RELEASE_TAG"
        docker push "$DOCKER_REPO:$RELEASE_TAG"
    fi;
}

function run_tests() {
    echo "Running tests..."

    if [ -z "$( which python3 )" ]; then
        echo "ERROR: To run test, python3 needs to be installed."
        exit 1
    fi

    if ! ( cd "$SCRIPT_DIR/tests" ; python3 -m unittest discover ); then
        echo "One or more tests failed. Exiting..."
        exit 1
    fi

    echo "Tests completed successfully!"
}

# Build image
build_container
# Run Tests
[ "$RUN_TESTS" == "1" ] && run_tests
# Determine Version of Image
[ "$APPLY_NAMING_SCHEME" == "1" ] && determine_jriver_version
# Publish latest and stable Image
[ "$PUBLISH_IMAGES" == "1" ] && [ "$DEB_URL" == "repository" ] && publish_rolling
[ "$PUBLISH_IMAGES" == "1" ] && [ "$APPLY_NAMING_SCHEME" == "1" ] && publish_versioned