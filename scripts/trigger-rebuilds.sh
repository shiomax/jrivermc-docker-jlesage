#!/bin/bash

# load variable from config file
source versions/"$1"
image_version=$(cat rootfs/VERSION)

function docker_tag_exists() {
    curl --silent -f -lSL https://hub.docker.com/v2/repositories/$CI_REGISTRY_IMAGE_NAME/tags/$1 &> /dev/null
}

./scripts/find_versions.sh --channel "v$JRIVER_RELEASE/$JRIVER_TAG" --version "$JRIVER_RELEASE" --min "$REBUILD_MIN" --max "$REBUILD_MAX" --arch "$ARCH" | while read line; do
    jriver_version="$(echo $line | awk '{print $2}')"
    image_tag="$jriver_version-$JRIVER_TAG-$image_version"
    if ! docker_tag_exists $image_tag && ! grep -q $jriver_version ./scripts/rebuild-blacklist.txt; then
        echo "Image $image_tag does no yet exist, triggering rebuild..."

        deb_url=$(echo $line | awk '{print $3}')

        echo "Variables"
        echo "BRANCH: $CI_BUILD_REF_NAME"
        echo "CI_REGISTRY_IMAGE: $CI_REGISTRY_IMAGE"
        echo "CI_BUILD_VERSION: $CI_BUILD_VERSION"
        echo "CI_REGISTRY_IMAGE_NAME: $CI_REGISTRY_IMAGE_NAME"
        echo "CI_DEB_URL: $deb_url"

        curl -X POST \
            -H "Content-Type: multipart/form-data" \
            -F "token=$CI_TRIGGER_TOKEN" \
            -F "ref=$CI_BUILD_REF_NAME" \
            -F "variables[CI_REGISTRY_IMAGE]=$CI_REGISTRY_IMAGE" \
            -F "variables[CI_REGISTRY_IMAGE_NAME]=$CI_REGISTRY_IMAGE_NAME" \
            -F "variables[CI_BUILD_VERSION]=$CI_BUILD_VERSION" \
            -F "variables[CI_DEB_URL]=$deb_url" \
            https://gitlab.com/api/v4/projects/17648034/trigger/pipeline
    fi;
done;