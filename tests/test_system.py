#! /usr/bin/python3

import unittest
from time import sleep
from test_base import TestBase

class TestSystem(unittest.TestCase, TestBase):

    def setUp(self):
        super(TestSystem, self).setupCommon()
        super(TestSystem, self).setUpContainer()

    def tearDown(self):
        super(TestSystem, self).tearDownContainer()

    def test_apt_working(self):
        while self.timeout > 0:
            try:
                returnCodeUpdate = super(TestSystem, self).dockerExec('apt-get update').returncode
                returnCodeUpgrade = super(TestSystem, self).dockerExec('apt-get upgrade -y').returncode
                super(TestSystem, self).dockerExec('apt-get install -y vim').returncode
                stdoutWhich = super(TestSystem, self).dockerExec('which vim').stdout
                
                if(returnCodeUpdate == 0 and returnCodeUpgrade == 0 and "/usr/bin/vim" in stdoutWhich):
                    break
            except Exception:
                pass
            finally:
                self.timeout-=1
                sleep(1)
        self.assertFalse(self.timeout == 0)