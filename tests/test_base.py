#! /usr/bin/python3

import subprocess
import os
from time import sleep

class TestBase(object):

    containerName = "jriver_test_container"
    timeout = None
    defaultTimeout = None

    def setupCommon(self):
        # Set Docker Repo
        self.dockerRepo = os.getenv("DOCKER_REPO")
        if not self.dockerRepo: self.dockerRepo = "shiomax/jrivermc27"
        # Set Docker Tag
        self.dockerTag = os.getenv("DOCKER_TAG")
        if not self.dockerTag: self.dockerTag = "latest"
        # Set Docker IP
        self.dockerIp = os.getenv("DOCKER_IP")
        if not self.dockerIp: self.dockerIp = "192.168.1.41"
        # Set Timeout
        self.timeout = int(os.getenv("TEST_TIMEOUT"))
        if not self.timeout: self.timeout = 30
        self.defaultTimeout = self.timeout
        # Set config directory
        self.configDir = os.getenv("JRIVER_CONFIG_DIR")
        # Set custom mac address
        self.macAddress = os.getenv("CONTAINER_MAC_ADDRESS")

    def setUpContainer(self, dockerExtraOpts=None):
        self.tearDownContainer()
        startCommand = ["docker", "run", 
            "-d", "--net=bridge",
            "--name=%s" % (self.containerName), 
            "-p", "5800:5800", 
            "-p", "5900:5900",
            "-p", "52199:52199",
            "-p", "52101:52101",
            "-p", "52100:52100",
            "-p", "1900:1900/udp"]

        # Mount config directory if configured
        if self.configDir: startCommand += ["-v", "%s:/config:rw" % self.configDir]

        # Set custom mac address if configured
        if self.macAddress: startCommand += ["--mac-address=%s" % self.macAddress]

        # Add extra options defined in individual tests
        if dockerExtraOpts: startCommand = startCommand + dockerExtraOpts

        startCommand += ["%s:%s" % (self.dockerRepo, self.dockerTag)]

        subprocess.run(startCommand, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

    def tearDownContainer(self):
        stopCommand = ["docker", "rm", "-f", self.containerName]
        
        findCommand = ["docker", "ps", "-a"]
        while self.containerName in subprocess.run(findCommand, encoding='utf-8', stdout=subprocess.PIPE).stdout:
            subprocess.run(stopCommand, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
            sleep(1)

    def dockerExec(self, cmd):
        command = ["docker", "exec", self.containerName] + cmd.split()
        completedProcess = subprocess.run(command, encoding='utf-8', stdout=subprocess.PIPE)
        return completedProcess 