#! /usr/bin/python3

import unittest
from time import sleep
from test_base import TestBase
import socket
import sys
import re

class TestUI(unittest.TestCase, TestBase):

    def setUp(self):
        super(TestUI, self).setupCommon()
        super(TestUI, self).setUpContainer()

    def tearDown(self):
        super(TestUI, self).tearDownContainer()

    def test_jriver_window_exists(self):
        self.assertTrue(self.getJRiverWindowID())

    def test_jriver_window_visible(self):
        id = self.getJRiverWindowID()
        self.assertTrue(id)
        timeout = self.defaultTimeout
        isVisible = False
        while timeout > 0 and isVisible == False:
            isVisible = self.isJRiverWindowVisible(id)
            sleep(1)
            timeout-=1
        self.assertTrue(isVisible)

    def test_jriver_window_minimize(self):
        id = self.getJRiverWindowID()
        self.assertTrue(id)
        sleep(10)
        for _ in range(10):
            timeout = self.defaultTimeout
            isVisible = False
            while timeout > 0 and isVisible == False:
                isVisible = self.isJRiverWindowVisible(id)
                sleep(1)
                timeout-=1
            self.assertTrue(isVisible)
            super(TestUI, self).dockerExec("xdotool windowactivate %s" % (id))
            super(TestUI, self).dockerExec("xdotool getactivewindow windowminimize")
            sleep(1)

    def isJRiverWindowVisible(self, windowId):
        cp = super(TestUI, self).dockerExec("xprop -id %s" % (windowId))
        if(cp.returncode != 0): return False
        states=None
        for line in cp.stdout.splitlines():
            if "_NET_WM_STATE(ATOM)" in line:
                states=line
                break
        if not states: return False
        return states.count("_NET_WM_STATE_HIDDEN") == 0

    def getJRiverWindowID(self):
        timeout = self.defaultTimeout

        while self.timeout > 0:
            cp = super(TestUI, self).dockerExec("wmctrl -l")
            if(cp.returncode == 0 and cp.stdout.count("JRiver Media Center") == 1):
                for line in cp.stdout.splitlines():
                    fields = line.strip().split()
                    if len(fields) > 5 and fields[3] == "JRiver" and fields[4] == "Media" and fields[5] == "Center":
                        return fields[0]
            sleep(1)
            timeout-=1
        
        return False