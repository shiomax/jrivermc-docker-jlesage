#! /usr/bin/python3

import unittest
from time import sleep
from test_base import TestBase

class TestOptions(unittest.TestCase, TestBase):

    def setUp(self):
        super(TestOptions, self).setupCommon()

    def tearDown(self):
        super(TestOptions, self).tearDownContainer()

    # MAXIMIZE_POPUPS should modify openbox config to maximize 'JRiver Popup Class' windows
    def test_maximize_popups(self):
        super(TestOptions, self).setUpContainer(dockerExtraOpts = ["-e", "MAXIMIZE_POPUPS=1"])

        while self.timeout > 0:
            try:
                cp = super(TestOptions, self).dockerExec("cat /etc/xdg/openbox/rc.xml")
                if cp.returncode == 0 and '<application type="normal" name="JRiver Popup Class">' in cp.stdout:
                    break
            finally:
                self.timeout-=1
                sleep(1)

        self.assertFalse(self.timeout == 0)

    # ENABLE_CJK_FONT options should install CJK font
    def test_enable_cjk_font(self):
        super(TestOptions, self).setUpContainer(dockerExtraOpts = ["-e", "ENABLE_CJK_FONT=1" ])

        while self.timeout > 0:
            try:
                cp = super(TestOptions, self).dockerExec("fc-list")
                if(cp.returncode == 0 and "wqy-zenhei" in cp.stdout):
                    break
            finally:
                self.timeout-=1
                sleep(1)

        self.assertFalse(self.timeout == 0)

    # MC should not start when using BLOCK_MC_STARTUP option
    def test_block_startup(self):
        super(TestOptions, self).setUpContainer(dockerExtraOpts = ["-e", "BLOCK_MC_STARTUP=1" ])

        while self.timeout > 0:
            try:
                cp = super(TestOptions, self).dockerExec("wmctrl -l")
                if(cp.returncode == 0 and cp.stdout.count("JRiver Media Center") == 1):
                    break
            finally:
                self.timeout-=1
                sleep(1)

        self.assertTrue(self.timeout == 0)

    # MC should start again when file '/tmp/BLOCK_MC_STARTUP' is removed
    def test_remove_block_startup(self):
        super(TestOptions, self).setUpContainer(dockerExtraOpts = ["-e", "BLOCK_MC_STARTUP=1" ])

        while self.timeout > 0:
            try:
                super(TestOptions, self).dockerExec("rm /tmp/BLOCK_MC_STARTUP")
                cp = super(TestOptions, self).dockerExec("wmctrl -l")
                if(cp.returncode == 0 and cp.stdout.count("JRiver Media Center") == 1):
                    break
            finally:
                self.timeout-=1
                sleep(1)

        self.assertFalse(self.timeout == 0)