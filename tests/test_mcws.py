#! /usr/bin/python3

import unittest
import urllib.request
from time import sleep
from test_base import TestBase

class TestMCWS(unittest.TestCase, TestBase):

    def setUp(self):
        super(TestMCWS, self).setupCommon()
        super(TestMCWS, self).setUpContainer()

    def tearDown(self):
        super(TestMCWS, self).tearDownContainer()

    def test_jriver_active(self):
        while self.timeout > 0:
            try:
                response = urllib.request.urlopen("http://%s:52199/MCWS/v1/Alive" % (self.dockerIp))
                data = response.read().decode('utf-8')
                if 'Status="OK"' in data:
                    break
            except Exception:
                pass
            finally:
                self.timeout-=1
                sleep(1)
        self.assertFalse(self.timeout == 0)