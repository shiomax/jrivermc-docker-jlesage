#! /usr/bin/python3

import unittest
from time import sleep
from test_base import TestBase
import socket

class TestSecurePorts(unittest.TestCase, TestBase):

    def setUp(self):
        super(TestSecurePorts, self).setupCommon()
        super(TestSecurePorts, self).setUpContainer(["-e", "SECURE_CONNECTION=1"])

    def tearDown(self):
        super(TestSecurePorts, self).tearDownContainer()

    def test_https_port_available(self):
        while self.timeout > 0:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = sock.connect_ex((self.dockerIp, 5800))
            sock.close()
            if result == 0:
                break
            self.timeout -= 1
            sleep(1)
        self.assertFalse(self.timeout == 0)

    def test_secure_vnc_port_available(self):
        while self.timeout > 0:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = sock.connect_ex((self.dockerIp, 5900))
            sock.close()
            if result == 0:
                break
            self.timeout -= 1
            sleep(1)
        self.assertFalse(self.timeout == 0)