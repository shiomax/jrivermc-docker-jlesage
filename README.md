**!!!**

**There is now a new image available at [shiomax/jrivermc](https://hub.docker.com/r/shiomax/jrivermc) that will effectively replace this one. For new installs it´s recommended you use the new image right away as this image eventually will stop being maintained / built.**

**!!!**

# JRiver Media Center - Docker

This is a community built docker image for [JRiver Media Center](https://jriver.com/). Images are based off [jlesage/baseimage-gui](https://github.com/jlesage/docker-baseimage-gui).

The sourcecode is available at [gitlab.com](https://gitlab.com/shiomax/jrivermc-docker-jlesage).

It's intended for use as a central 'headless' JRiver server you stream music from with any of your other JRiver instances.

Direct music playback from the container (can) be setup, but is dependent on having alsa running on the host (see Sound Section).

# Dockerhub

| JRiver Version | Repository | Still rebuilding |
|----------------|------------|------------------|
| Media Center 30 | [shiomax/jrivermc30](https://hub.docker.com/r/shiomax/jrivermc30) | yes |
| Media Center 29 | [shiomax/jrivermc29](https://hub.docker.com/r/shiomax/jrivermc29) | yes |
| Media Center 28 | [shiomax/jrivermc28](https://hub.docker.com/r/shiomax/jrivermc28) | yes |
| Media Center 27 | [shiomax/jrivermc27](https://hub.docker.com/r/shiomax/jrivermc27) | no |
| Media Center 26 | [shiomax/jrivermc26](https://hub.docker.com/r/shiomax/jrivermc26) | no |
| Media Center 25 | [shiomax/jrivermc25](https://hub.docker.com/r/shiomax/jrivermc25) | no |
| Media Center 24 | [shiomax/jrivermc24](https://hub.docker.com/r/shiomax/jrivermc24) | no |

# Screenshots

![screenshot](https://gitlab.com/shiomax/jrivermc-docker-jlesage/raw/master/assets/screenshot.png)

# How to use this

**Note about network_mode setting** For the most part it is recommended that you use **host** networking. Unless, you need the ability to change port mappings as some are already in use by your host. If you are using bridge networking it comes with a few caveats. For one you need to define a custom mac address, because otherwise you will be activating your JRiver instance a lot as their License does not appreciate your mac address changing. Also, one thing I´ve noticed. Network discovery does not quite work with bridge networking. Meaning your other JRiver instances may not discover this docker container as a zone.

### **Docker Compose**

I recommend [docker-compose](https://docs.docker.com/compose/). When using docker-compose you will create a configuration file ```docker-compose.yml``` that contains all the configuration for your container. This makes it easier to recreate the container with the exact same configuration. For instance when you want to change the version of the image you are using. You will need to destroy and recreate the container. If you do not use docker-compose you will need to keep track of what exact docker run command you used to recreate the container with the same configuration.

Bridge networking (default)

```yml
version: '3'
services:
  jrivermc28:
    image: shiomax/jrivermc28
    restart: always
    container_name: jrivermc28
    mac_address: ed:e8:60:2d:65:c1
    ports:
      - "5800:5800"
      - "5900:5900"
      - "52100:52100"
      - "52101:52101"
      - "52199:52199"
      - "1900:1900/udp"
    environment:
      - VNC_PASSWORD=12345
    volumes:
        - /path/to/config:/config:rw
        - /path/to/music:/data/music:rw
```

Or use host network.

```yml
version: '3'
services:
  jrivermc28:
    image: shiomax/jrivermc28
    restart: always
    container_name: jrivermc28
    network_mode: host
    environment:
      - VNC_PASSWORD=12345
    volumes:
        - /path/to/config:/config:rw
        - /path/to/music:/data/music:rw
```

To run the container ```docker-compose up -d```

Updating the container
- ```docker-compose pull``` to pull the latest image as specified in the compose file
- ```docker-compose down``` to stop and remove the container (does not mean your data is gone, assuming you have mounted your /config volume)
- ```docker-compose up -d``` to create the container and detach from the screen
- ```docker image prune``` to cleanup unused images

### **Without Docker Compose**

Bridge networking (default)

```sh
docker run -d \
    --name=jrivermc28 \
    --net=bridge \
    --restart=always \
    --mac-address=ed:e8:60:2d:65:c1 \
    -p 5800:5800 \
    -p 5900:5900 \
    -p 52199:52199 \
    -p 52101:52101 \
    -p 52100:52100 \
    -p 1900:1900/udp \
    -v config:/config:rw \
    -v /path/to/music:/data/music:rw \
    -e VNC_PASSWORD=12345 \
    shiomax/jrivermc28
```

Or use host network.

```sh
docker run -d \
    --name=jrivermc28 \
    --net=host \
    --restart=always \
    -v config:/config:rw \
    -v /path/to/music:/data/music:rw \
    -e VNC_PASSWORD=12345 \
    shiomax/jrivermc28
```

Updating the container
- ```docker pull shiomax/jrivermc28:latest``` pull a new image with tag latest
- ```docker rm -f jrivermc28``` stop and remove the container with name jrivermc28
- ```docker run -d ....``` rerun the entier run command you ran to initially create the container
- ```docker image prune``` to cleanup unused images

# Image Tags


- ```shiomax/jrivermc28:latest``` 

  Using JRiver version from the 'latest' repository (this is also what you get without specifying anything)
- ```shiomax/jrivermc25:stable```
  
  Using JRiver version from the 'stable' repository, but it's otherwise the same

- Tags like ```25.0.83-latest-1``` ```25.0.50-stable-1``` ```25.0.80-latest-0 ```

Those are copies of ```stable``` and ```latest``` tags at the time. You can use those tags if a new version of the image
causes problems to go back in time. These images are pushed once and never touched again.
The tags contain the JRiver Version (25.0.83), the JRiver repository (latest) and the image version (1).

# Volumes (Bind mounts)

Volumes:
- ```/config``` that's the home directory of the container, definitely mount this to an external directory, so your settings and so forth are persisted properly. Your library backups will also be in there. As long as you have this folder you can destroy and recreate the container all you want and JRiver (should) come back up as if nothing ever happened. If you don´t have this folder and you delete the container, then everything you may have done is gone. For temporary installs (testing purposes) it´s fine to not mount anything to it though.
- ```/data/music``` You can mount your music to ```/data/music```. You can also use any other folder to mount your media to. If you don't know where to put them, just use ```/data/music``` too.

# Ports

Port mappings:
- Ports used by JRiver: 52100, 52101, 52199, 1900/udp
- Port for Web GUI: 5800
- Port for VNC access: 5900 (if you're only gonna use the web GUI you don't need this)

# Sound

You can map your alsa sound device ```/dev/snd``` between the host and container to enable sound playback from within the container.

- When using the docker run command ```--device /dev/snd``` as an option.

- When using docker compose add the following to your ```docker-compose.yml```.

```yml
devices:
  - "/dev/snd"
```

NOTE: this will not work prior to version 7.

# Accessing the GUI

You can either access the GUI from a web browser or from a VNC client.

Assuming you did not remap the ports.

- Web UI 
```
http://<HOST IP ADDR>:5800
```
- Web UI (with ```SECURE_CONNECTION``` enabled)
```
https://<HOST IP ADDR>:5800
```

- VNC
```
<HOST IP ADDR>:5900
```

- VNC (with ```SECURE_CONNECTION``` enabled)
```
<HOST IP ADDR>:5900
```
NOTE: You need a VNC client that supports SSL encryption, such as SSVNC. Many VNC clients will not work when you turn on the ```SECURE_CONNECTION``` flag.

# Activate JRiver using .mjr file

If the key does not work for any reason, you can use a .mjr file from [https://rover.jriver.com/cgi-bin/restore.cgi](https://rover.jriver.com/cgi-bin/restore.cgi).
Enter your license key there. Download the .mjr file. The file is valid for 14 days. Within those 14 days you have to activate your jriver instance.

1. Place the .mjr file into the config directory
2. Log into the container with bash ```docker exec -it <container_name> /bin/bash```
3. Activate JRiver (setting $HOME first is important, otherwise your bash session will use a temporary home directory and not the same as JRiver)
```
export HOME=/config 
mediacenter28 /RestoreFromFile /config/<mjr_file_name>
```
4. JRiver should now be activated. You can delete the .mjr file now.

**NOTE:** As of writing this the ```/RestoreFromFile``` command does not work if MC is running. Running the command will only kill MC, but the activation will fail. MC is autostarted by the container if it exits for any reason. If you are at least on Version 16 of the image you can block MC from starting by either setting ```BLOCK_MC_STARTUP``` to ```1``` on container creation or use ```touch /tmp/BLOCK_MC_STARTUP``` and close MC afterwards inside a running container. Removing the file will allow MC to start again (```rm /tmp/BLOCK_MC_STARTUP```). Do this before trying to activate MC.

# Permission problems

If you are having permission problems in your container and cannot access your media there are multiple ways to solve this.

Changing the permissions or/and ownership on your music folder would be one way to do that. But instead you can also change what user JRiver will run as inside the container by changing the ```GROUP_ID``` and ```USER_ID``` environment variables.

Those two are numeric values and their default value is ```1000```, which on a generic Linux install is the id of the first user you created. If your music folder belongs to another user you can change these values.

1. Find out what user and group the folder belongs to
```
ls -la /path/to/music
```
2. Use the id command to find out the numeric number for that user
```
id <username>
```
3. Adjust your container configuration accordingly.

With docker-cli:
```sh
docker run -d \
    --name=jrivermc28 \
    --net=host \
    --restart=always \
    -v config:/config:rw \
    -v /path/to/music:/data/music:rw \
    -e USER_ID=1234 \
    -e GROUP_ID=1234 \
    -e VNC_PASSWORD=12345 \
    shiomax/jrivermc28
```

With docker-compose:
```yml
version: '3'
services:
  jrivermc28:
    image: shiomax/jrivermc28
    restart: always
    container_name: jrivermc28
    network_mode: host
    environment:
      - VNC_PASSWORD=12345
      - USER_ID=1234
      - GROUP_ID=1234
    volumes:
        - /path/to/config:/config:rw
        - /path/to/music:/data/music:rw
```

# Firewall-Configuration (only relevant for host networking)

This is usually only necessary if you are using host networking. Bridge networking by default does not need any firewall configuration. As docker-proxy is already allowed to go threw it when you install docker.

### firewalld (centos, fedora or rhel)

1. Login as root ```sudo su``` (ALL of these things need root access, or don't and prepend sudo to every command)
2. Create a file ```nano /etc/firewalld/services/jriver.xml```
```
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>jriver</short>
  <description>Ports required by JRiver MediaCenter.</description>
  <port protocol="tcp" port="5800"></port>
  <port protocol="tcp" port="52199"></port>
  <port protocol="tcp" port="52100"></port>
  <port protocol="tcp" port="52101"></port>
  <port protocol="tcp" port="5900"></port>
  <port protocol="udp" port="1900"></port>
</service>
```
3. Reload firewalld ```firewall-cmd --reload```
4. Add configuration temporarily ```firewall-cmd --add-service=jriver --zone=public```
5. Try if you can access jriver
6. If yes, make the configuration permanent 
    - ```firewall-cmd --add-service=jriver --zone=public --permanent```
    - ```firewall-cmd --reload```
7. If no, you'll have to figure that out. Since the configuration was only temporarily applied another reload will make it go away.

### ufw (ubuntu or debian *if you install ufw*)

1. Login as root ```sudo su``` or ```su``` on debain (ALL of these things need root access, otherwise prepend sudo to every command)
2. Create a file ```nano /etc/ufw/applications.d/jriver```
```
[jriver]
title=jriver
description=Ports required by JRiver MediaCenter
ports=5800,52199,52100,52101,5900/tcp|1900/udp
```
3. Update the app profile ```ufw app update jriver``` after you can check your configuration with ```ufw app info jriver```
4. Allow the app ```ufw allow jriver```

# Running custom initialization Tasks

NOTE: For advanced users only.

A good way to run custom initialization logic on startup is to write an init script and mount it into ```/etc/cont-init.d```. You can do whatever you want like installing additional packages or applying fixes for broken things. Although, if you make changes like this that could help other users of the image I would encourage you to try and submit those changes (make a merge request on gitlab.com) so that those changes are available for everyone and not just you alone.

Scripts in ```/etc/cont.init.d``` are executed on container startup ordered by the proceeding number. For instance ```10-max-popup.sh``` currently looks like this

```
#!/usr/bin/with-contenv sh

set -e # Exit immediately if a command exits with a non-zero status.
set -u # Treat unset variables as an error.

log() {
    echo "[cont-init.d] $(basename $0): $*"
}

if [ "$MAXIMIZE_POPUPS" = "1" ]; then
    log "Applying max popup config to openbox."
    sed -i '/<\/applications>/{
        r /templates/max-popups.xml
        a \</applications>
        d
    }' /etc/xdg/openbox/rc.xml
else
    log "Popups won't be maximized"
fi;
```

Nothing special applies a change in the openbox config to make popup windows be fullscreen. You could do anything with those scripts. Write them and then mount them into the container just like you would any other file. Just like you mount your music directory. Only now you are just gonna mount a single file. I do recommend using a big number in front (like 100) so that your script runs after all the other initialization logic.

# Environment Variables

Specific to this image

| Variable       | Description                                  | Default |
|----------------|----------------------------------------------|---------|
| `MAXIMIZE_POPUPS` | When set to `1` maximizes MC popup Windows. | `0` |
|`BLOCK_MC_STARTUP`| When set to `1` MC will be blocked from starting until `/tmp/BLOCK_MC_STARTUP` is removed. | `unset` |

Those are inherited from the base image [jlesage/baseimage-gui](https://github.com/jlesage/docker-baseimage-gui). Some have their default values changed.

| Variable       | Description                                  | Default |
|----------------|----------------------------------------------|---------|
|`APP_NAME`| Name of the application. | `JRiver MediaCenter 25` |
|`USER_ID`| ID of the user the application runs as.  See [User/Group IDs](https://github.com/jlesage/docker-baseimage-gui#usergroup-ids) to better understand when this should be set. | `1000` |
|`GROUP_ID`| ID of the group the application runs as.  See [User/Group IDs](https://github.com/jlesage/docker-baseimage-gui#usergroup-ids) to better understand when this should be set. | `1000` |
|`SUP_GROUP_IDS`| Comma-separated list of supplementary group IDs of the application. | (unset) |
|`UMASK`| Mask that controls how file permissions are set for newly created files. The value of the mask is in octal notation.  By default, this variable is not set and the default umask of `022` is used, meaning that newly created files are readable by everyone, but only writable by the owner. See the following online umask calculator: http://wintelguy.com/umask-calc.pl | (unset) |
|`TZ`| [TimeZone] of the container.  Timezone can also be set by mapping `/etc/localtime` between the host and the container. | `Etc/UTC` |
|`KEEP_APP_RUNNING`| When set to `1`, the application will be automatically restarted if it crashes or if user quits it. | `1` |
|`APP_NICENESS`| Priority at which the application should run.  A niceness value of -20 is the highest priority and 19 is the lowest priority.  By default, niceness is not set, meaning that the default niceness of 0 is used.  **NOTE**: A negative niceness (priority increase) requires additional permissions.  In this case, the container should be run with the docker option `--cap-add=SYS_NICE`. | (unset) |
|`TAKE_CONFIG_OWNERSHIP`| When set to `1`, owner and group of `/config` (including all its files and subfolders) are automatically set during container startup to `USER_ID` and `GROUP_ID` respectively. | `1` |
|`CLEAN_TMP_DIR`| When set to `1`, all files in the `/tmp` directory are delete during the container startup. | `1` |
|`DISPLAY_WIDTH`| Width (in pixels) of the application's window. | `1280` |
|`DISPLAY_HEIGHT`| Height (in pixels) of the application's window. | `768` |
|`SECURE_CONNECTION`| When set to `1`, an encrypted connection is used to access the application's GUI (either via web browser or VNC client).  See the [Security](https://github.com/jlesage/docker-baseimage-gui#security) section for more details. | `0` |
|`VNC_PASSWORD`| Password needed to connect to the application's GUI.  See the [VNC Password](https://github.com/jlesage/docker-baseimage-gui#vnc-password) section for more details. | (unset) |
|`X11VNC_EXTRA_OPTS`| Extra options to pass to the x11vnc server running in the Docker container.  **WARNING**: For advanced users. Do not use unless you know what you are doing. | (unset) |
|`ENABLE_CJK_FONT`| When set to `1`, open source computer font `WenQuanYi Zen Hei` is installed.  This font contains a large range of Chinese/Japanese/Korean characters. | `0` |

# Changelog

### Version 18
- Add Images for MC30

### Version 17
- Add Images for MC29
- All init scripts renamed to ```10-...``` because order does not matter, can run at the same time

### Version 16
- Fix apt not working out of the box 'unknown system group 'messagebus' in statoverride file'
- Added test for apt to ensure it will work in future versions
- Added test for ```ENABLE_CJK_FONT``` options
- Added option to block startup of MC to make it easier to activate using .mjr file

### Version 15
- Update baseimage to v3.5.8

### Version 14
- Install ffmpeg to get video import working

### Version 13
- Updated Baseimage to v3.5.7
- JRiverMC 28 builds

### Version 12
- Added build targets for ```beta``` versions
- Fixed ```ENABLE_CJK_FONT``` not installing the font

### Version 11
- Install ```libgbm1``` to make browser work on JRiverMC27

### Version 10
- Repository information was outdated in startapp.sh
- JRiverMC27 images

### Version 9
- Updated baseimage to ```jlesage/baseimage-gui:debian-10-v3.5.6```

### Version 8
- Added openbox dependency to xpropspy
- Repository moved to [gitlab.com](https://gitlab.com/shiomax/jrivermc-docker-jlesage)
- Making sure Window is visible on startup
- Actually using ```--deb-url``` added in v7 to rebuild old JRiver versions with new image version

### Version 7
- Add ```audio``` group to application when mapping ```/dev/snd``` (alsa sound device) between host and the container (so the container can play sound)
- Added some metadata into builds
- More and improved tests
- New buildscript option ```--deb-url``` to be able to build images for older JRiver versions.
- amd64 builds now based on ```jlesage/baseimage-gui:debian-10-v3.5.3``` as it´s now available.

### Version 6
- Now based upon a rebuilt baseimage for debian10 shiomax/docker-baseimage-gui
- As JRiver now properly keeps sub-windows on top, xpropspy now only brings back the main Window when minimized (also works a lot better, mostly because it's just simpler to do just that)
- New buildscript with several options, also builds for arm (not in the dockerhub repo yet)
- Removed ```ENABLE_SIGNATURE``` option now just prints out version on start without any silly drawings

### Version 5
- Removed mjractivate script again (lookup "Activate JRiver using .mjr file" section on how to activate jriver via cli, if it does not activate the normal way)

### Version 4
- ~~Automatically activate JRiver with '.mjr' file~~ Removed again in V5

### Version 3
- Updated baseimage from debian9:3.5.2 to debian9:3.5.3

### Version 2
- xpropspy now uses sh instead of bash (microoptimization sh is faster, but can do less)
- ascii art on startup and environment variable ENABLE_SIGNATURE (1 by default)
- specifying debian-9-v3.5.2 instead of latest, witch is currently equivalent (to avoid breaking the image on automated builds)
- removed layer below for main jriver window from rc.xml as no longer needed (also seems to have fixed some ui gliches in the integrated webgui)
- added option MAXIMIZE_POPUPS to maximize every window that has the name "JRiver Popup Class" (0 by default)

### Version 1
- added image version
